﻿using Dapper;
using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FuncOrm.FuncOrm
{
    /// <summary>
    /// Statisk klasse som genererer CRUD statements basert på generic klasse 
    /// og kjørere disse vha Dapper
    /// Merk Ingen properties og inten Mutering av variable
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public static class FncQry<T>
    {
        // ----------------------------------------------------------------------------------------
        // Fields lists
        // ----------------------------------------------------------------------------------------

        // OldSchool: function GetIdField (TblDef tableDef) {
        //               return tableDef.Fields.First().FieldName
        //            } 
        // Lambda:    GetIdField (TblDef tableDef) => tableDef.Fields.First().FieldName
        public static Func<TblDef, string> GetIdField = (TblDef tableDef) => tableDef.Fields.First().FieldName;
        public static Func<TblDef, List<String>> GetNonIdFields = (TblDef tableDef) => tableDef.Fields.Select((x) => x.FieldName).Skip(1).ToList();
        public static Func<TblDef, List<String>> GetAllFields = (TblDef tableDef) => tableDef.Fields.Select((x) => x.FieldName).ToList();

        // ----------------------------------------------------------------------------------------
        // Generators for SQL Statements
        // ----------------------------------------------------------------------------------------

        public static Func<TblDef, string> GetCreateStatement = (TblDef tableDef) =>
             $"CREATE TABLE IF NOT EXISTS {tableDef.TableName} ("
                + GetIdField(tableDef) + " Integer primary key "
                + tableDef.Fields.Skip(1).Aggregate("", (res, x) => res + $", {x.FieldName} {x.DataType} ")
                + ")";

        public static Func<TblDef, string> GetSelectStatement = (TblDef tableDef) => $"SELECT {string.Join(", ", GetAllFields(tableDef))} from {tableDef.TableName}";
        public static Func<TblDef, string> GetSelectStatementById = (TblDef tableDef) => $"SELECT {string.Join(", ", GetAllFields(tableDef))} from {tableDef.TableName} where {GetIdField(tableDef)} = @{GetIdField(tableDef)}";
        public static Func<TblDef, String, string> GetSelectStatementByFieldName = (TblDef tableDef, String fldName) => $"SELECT {string.Join(", ", GetAllFields(tableDef))} from {tableDef.TableName} where {fldName} = @Value";
        public static Func<TblDef, string> GetSelectStatementByIdIn = (TblDef tableDef) => $"SELECT {string.Join(", ", GetAllFields(tableDef))} from {tableDef.TableName} where {GetIdField(tableDef)} in @{GetIdField(tableDef)}s";
        public static Func<TblDef, string> GetInsertStatement = (TblDef tableDef) => $"INSERT INTO {tableDef.TableName} ({string.Join(", ", GetNonIdFields(tableDef))}) VALUES ({string.Join(", ", GetNonIdFields(tableDef).Select((x) => $"@{x}"))}); select last_insert_rowid()";
        public static Func<TblDef, string> GetUpdateStatement = (TblDef tableDef) => $"UPDATE {tableDef.TableName} SET {string.Join(", ", GetNonIdFields(tableDef).Select((x) => $"{x} = @{x}"))} WHERE {GetIdField(tableDef)} = @{GetIdField(tableDef)}";
        public static Func<TblDef, string> GetDeleteStatement = (TblDef tableDef) => $"DELETE FROM {tableDef.TableName} WHERE {GetIdField(tableDef)} = @Id";

        // ----------------------------------------------------------------------------------------
        // DB driver calls
        // ----------------------------------------------------------------------------------------

        public static List<T> DbQuery(IDbConnection Db, String Query, Object Param = null) => Db.Query<T>(Query, param: Param).ToList();        
        public static Int64 DbQueryInt(IDbConnection Db, String Query, Object Param = null) => Db.Query<Int64>(Query, param: Param).First();
        public static Int64 DbExec(IDbConnection Db, String Query, Object Param = null) => Db.Execute(Query, param: Param);

        //Int64 Id = Db.Query<Int64>(GetInsertSql(), (T)newObject, Transaction).First();

    }

    /// <summary>
    /// Generic klasse som tilbyr CRUD
    /// Merk Connstring og TableDef kan kun settes gjennom constructor,
    /// blir tilsvarende til Closure i funksjonelle språk, mao de kan ikke tukles med
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class FuncOrm<T>
    {
        private String Connstring { get; set; }
        private IDbConnection Db { get; set; }
        private readonly TblDef TableDef = null;
        public FuncOrm(String connString, TblDef tableDef)
        {
            Connstring = connString;
            TableDef = tableDef;

            Db = new SqliteConnection(connString);
        }

        // ----------------------------------------------------------------------------------------
        // Public API
        // ----------------------------------------------------------------------------------------        

        public IDbTransaction GetTransaction() => Db.BeginTransaction();       

        public List<T> SelectAll() => FncQry<T>.DbQuery(Db, FncQry<T>.GetSelectStatement(TableDef));
        public List<T> SelectById(int Id) => FncQry<T>.DbQuery(Db, FncQry<T>.GetSelectStatementById(TableDef), new DynamicParameters(new { Id }));
        public List<T> SelectByField(String FieldName, String Value) => FncQry<T>.DbQuery(Db, FncQry<T>.GetSelectStatementByFieldName(TableDef, FieldName), new { Value });
        public List<T> SelectWhereIdIn(List<int> Ids) => FncQry<T>.DbQuery(Db, FncQry<T>.GetSelectStatementByIdIn(TableDef), new { Ids });
        public void CreateTable() => FncQry<T>.DbExec(Db, FncQry<T>.GetCreateStatement(TableDef));
        public Int64 Insert(T parm) => FncQry<T>.DbQueryInt(Db, FncQry<T>.GetInsertStatement(TableDef), parm);
        public Int64 Update(T parm) => FncQry<T>.DbExec(Db, FncQry<T>.GetUpdateStatement(TableDef), parm);
        public Int64 Delete(int Id) => FncQry<T>.DbExec(Db, FncQry<T>.GetDeleteStatement(TableDef), new { Id });
    }
}
