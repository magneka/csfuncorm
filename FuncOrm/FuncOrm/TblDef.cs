﻿using System;
using System.Collections.Generic;

namespace FuncOrm.FuncOrm
{
    public class TblDef
    {
        public String TableName { get; set; }
        public List<TbFld> Fields { get; set; }
        public TblDef()
        {
            //TableName = tableName;
            Fields = new List<TbFld>();
        }
    }
}
