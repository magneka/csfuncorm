﻿using System;

namespace FuncOrm.FuncOrm
{
    public class TbFld
    {
        public String FieldName { get; set; }
        public String DataType { get; set; }
    }
}
