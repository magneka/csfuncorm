﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Dapper;
using FuncOrm.FuncOrm;
using Microsoft.Data.Sqlite;

namespace FuncOrm
{
    public class TestDto
    {
        public int Id { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public DateTime Updated { get; set; }

        public override String ToString() { return $"Id {Id}, Firstname {FirstName}, LastName:{LastName}, Updated:{Updated}"; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var testTable = new TblDef
            {
                TableName = "Test",
                Fields = new List<TbFld>
                {
                    new TbFld { FieldName = "Id", DataType = "Integer" },
                    new TbFld { FieldName = "FirstName", DataType = "Varchar(50)" },
                    new TbFld { FieldName = "LastName", DataType = "Varchar(50)" },
                    new TbFld { FieldName = "Updated", DataType = "DateTime" }
                }
            };

            var TestRepository = new FuncOrm<TestDto>("Data Source=../../../demodb.db", testTable);

            TestRepository.CreateTable();

            var newRec = new TestDto { FirstName = "Balle", LastName = "Klorin", Updated = DateTime.Now };
            newRec.Id = (Int32)TestRepository.Insert(newRec);


            Debug.WriteLine("NewRec: ", newRec.ToString());

            var single = TestRepository.SelectById(1);


            var inlist = TestRepository.SelectWhereIdIn(new List<int> { 1, 2, 3 });
            DumpDto("inlist", inlist);

            var SelBy = TestRepository.SelectByField("FirstName", "Balle");
            DumpDto("SelBy", SelBy);

            newRec.FirstName = "Forman Supperådet, Balle";
            var Upd = (Int32)TestRepository.Update(newRec);

            var all = TestRepository.SelectAll();
            DumpDto("all", all);

            var delcnt = TestRepository.Delete(newRec.Id);

            var all2 = TestRepository.SelectAll();
            DumpDto("all2", all2);
        }

        private static void DumpDto(String prefix, List<TestDto> inlist)
        {
            foreach (var item in inlist)
            {
                Debug.WriteLine(item.ToString(),prefix);
            }
        }
    }
}
